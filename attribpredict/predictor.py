from __future__ import print_function

from attribpredict.visaparser import read_visa_dataset, read_visa_dataset_file

import sys
import os
import shutil
import urllib2
import random
import h5py

from collections import defaultdict, Counter

from base64 import b64encode

import skimage.io
import skimage.transform
import numpy as np
#import keras
#import keras.backend as K
#from keras.applications.vgg19 import VGG19
#import theano

import socket
import tarfile as tar

import requests


from sklearn.svm import SVC, LinearSVC
import sklearn.metrics

from sklearn.externals import joblib


def create_attribute_synset_map(concepts):
    attribute_synset_map = defaultdict(set)
    for concept_descr, attributes in concepts.items():
        name, synset = concept_descr
        if synset is None:
            continue
        for attrib in attributes:
            attribute_synset_map[attrib].add(synset)

    return attribute_synset_map

def find_top_n_attributes(attribute_synset_map, n = 1):
    sorted_attribs = sorted(attribute_synset_map.items(), key = lambda x: len(x[1]), reverse = True)

    return sorted_attribs[:n]

def get_image_files_in_dir(directory):
    img_filenames = []
    for filename in os.listdir(directory):
        if filename.endswith(".JPEG"):
            img_filenames.append(os.path.join(directory, filename))

    return img_filenames
        
def load_images_for_synsets(synsets, n = 100):
    synset_image_map = {}

    for synset in synsets:
        synset_image_map[synset] = load_images_for_synset(synsets, n)

    return synset_image_map
        

def load_images_for_synset(synset, n = 100):
    img_dir = "images/" + synset

    if not os.path.isdir(img_dir):
        os.mkdir(img_dir)

    img_filenames = get_image_files_in_dir(img_dir)

    if len(img_filenames) < n:
        new_img_filenames = download_images_for_synset(synset, img_dir, n - len(img_filenames))
        img_filenames.extend(new_img_filenames)

    if len(img_filenames) == 0:
        return []

    return read_images(img_filenames[:n])



import tarfile

def download_images_for_synset(synset, target_dir, n = 100):
    url = "http://www.image-net.org/download/synset?wnid={}&username=juliussteen&accesskey=f5129c01d8a2fdae157b33a4b4df5c0994e123a5&release=latest&src=stanford".format(synset)

    print(url, target_dir)
    handle = urllib2.urlopen(url)
    temp_name = os.path.basename(url)

    with open(temp_name, "wb") as f_out:
        f_out.write(handle.read())

    with tarfile.open(temp_name) as f_tar:
        f_tar.extractall(target_dir)

    images = list(map(lambda x: os.path.join(target_dir, x), filter(lambda x: x.endswith(".JPEG"), os.listdir(target_dir))))

    for img_name in images:
        img = skimage.io.imread(img_name)
        if len(img.shape) == 2:
            continue
        img = skimage.transform.resize(img, (224, 224))
        skimage.io.imsave(img_name, img)

    return images





#    image_urls = urllib2.urlopen("http://image-net.org/api/text/imagenet.synset.geturls?wnid=" + synset).read().strip().split("\n")

#    return download_images_from_urls(image_urls, target_dir, n)

def download_images_from_urls(urls, target_dir, n, dims = (224, 224)):
    img_filenames = []
    socket.setdefaulttimeout(1)
    
    for url in urls:
        fname = os.path.join(target_dir, b64encode(url))
        fname += ".jpeg"
        if os.path.isfile(fname):
            continue
        print(url)
        try:
            
            img = skimage.io.imread(url)
            img = skimage.transform.resize(img, dims)
            skimage.io.imsave(fname, img)

            img_filenames.append(fname)
        except:
            pass

        if len(img_filenames) >= n:
            break

    socket.setdefaulttimeout(60)

    return img_filenames
        

def convert_to_training_data(synset_image_map, pos_synsets, neg_synsets, n_max = 250):
    all_images = []
    labels = []

    synset_membership = []
    for synset in pos_synsets:
        if synset in synset_image_map:
            synsets_images = synset_image_map[synset]
            synsets_images = synsets_images[:n_max]
            all_images.append(synsets_images)
            for _ in range(synsets_images.shape[0]):
                labels.append(1)
                synset_membership.append(synset)

    for synset in neg_synsets:
        if synset in synset_image_map:
            synsets_images = synset_image_map[synset]
            synsets_images = synsets_images[:n_max]
            all_images.append(synsets_images)
            for _ in range(synsets_images.shape[0]):
                labels.append(0)
                synset_membership.append(synset)


    X = np.concatenate(all_images)
    y = np.array(labels)

    return X, y, np.array(synset_membership)


def train_test_split_synsets(synsets, splitratio = 0.2):
    synset_list = list(synsets)
    random.shuffle(synset_list)
    splitpoint = int(len(synset_list) * 0.2)

    return synset_list[splitpoint:], synset_list[:splitpoint]


def stratified_subsample(X, y, train_synset_membership):
    smaller_instance_count = (np.nonzero(y == 0))[0].shape[0]
    pos_instance_count = (np.nonzero(y == 1))[0].shape[0]

    if pos_instance_count < smaller_instance_count:
        smaller_instance_count = pos_instance_count
        

    X_samples = []
    y_samples = []
    train_synset_membership_samples = []
    for i in (0, 1):
        mask = np.nonzero(y == i)[0]
        instance_indices = np.random.choice(mask, smaller_instance_count)
        X_samples.append(X[instance_indices])
        y_samples.append(y[instance_indices])
        train_synset_membership_samples.append(train_synset_membership[instance_indices])

    return np.concatenate(X_samples), np.concatenate(y_samples), np.concatenate(train_synset_membership_samples)

class AttributeClassifier:
    def __init__(self):
        self.classification_threshold = None

    def fit(self, synset_embeddings_map, positive_synsets, negative_synsets):
        self.positive_synsets = positive_synsets
        self.negative_synsets = negative_synsets

        X_train, y_train, train_image_synset_map = convert_to_training_data(synset_embeddings_map, postive_synsets_train, negative_synsets_train)
        X_train, y_train, train_image_synset_map = stratified_subsample(X_train, y_train, train_image_synset_map)

        self.clf = LinearSVC()
        self.clf.fit(X_train, y_train)

    def predict_scores(self, synset_embeddings_map, synsets):
        X_test, y_test, image_synset_map = convert_to_training_data(synset_embeddings_map, synsets, set())
        predicted = self.clf.predict(X_test)

        scores = {}
        for synset in synsets:
            synset_scores = predicted[image_synset_map == synset]

            scores[synset] = np.sum(synset_scores) / float(synset_scores.shape[0])

        return scores

    def predict_classification(self, synset_embeddings_map, synsets, use_fixed_threshold = False, include_score = False):
        if self.classification_threshold is None:
            raise RuntimeError("Threshold must be tuned before use")

        scores = self.predict_scores(synset_embeddings_map, synsets)

        threshold = self.classification_threshold

        if use_fixed_threshold:
            threshold = 0.5

        results = set()

        for synset, score in scores.items():
            if score > threshold:
                if include_score:
                    results.add((synset, score))
                else:
                    results.add(synset)

        return results

    def evaluate_classifier_performance(self, synset_embeddings_map, positive_synsets, negative_synsets):
        X_train, y_train, train_image_synset_map = convert_to_training_data(synset_embeddings_map, postive_synsets_train, negative_synsets_train)
        X_train, y_train, train_image_synset_map = stratified_subsample(X_train, y_train, train_image_synset_map)        

        y_pred = self.clf.predict(X_train)

        return sklearn.metrics.f1_score(y_train, y_pred), sklearn.metrics.recall_score(y_train, y_pred), sklearn.metrics.precision_score(y_train, y_pred)


    def tune_threshold(self, synset_embeddings_map, positive_synsets, negative_synsets):
        positive_synsets = set(positive_synsets)
        negative_synsets = set(negative_synsets)

        if len(positive_synsets) < len(negative_synsets):
            negative_synsets = set(random.sample(negative_synsets, len(positive_synsets)))
        else:
            positive_synsets = set(random.sample(positive_synsets, len(negative_synsets)))
        scores = self.predict_scores(synset_embeddings_map, positive_synsets.union(negative_synsets))

        return self._tune_threshold_on_scores(scores, positive_synsets, negative_synsets)

    def _tune_threshold_on_scores(self, scores, positive_synsets, negative_synsets):
        max_f1 = 0.0
        best_threshold = 0.0
        curr_max_best_threshold = None
        #print(scores, positive_synsets)
        for interval in range(10):
            testing_threshold = interval / 10.0

            predicted_positive_synsets = set()
            for synset, score in scores.items():
                if score > testing_threshold:
                    predicted_positive_synsets.add(synset)
            _, _, f1 = calculate_rec_prec_f1(positive_synsets, negative_synsets, predicted_positive_synsets)
            f1 = calculate_accuracy(positive_synsets, negative_synsets, predicted_positive_synsets)

            if f1 > max_f1:
                max_f1 = f1
                best_threshold = testing_threshold
                curr_max_best_threshold = None
            elif abs(f1 - max_f1) < 0.01:
                curr_max_best_threshold = testing_threshold

        if curr_max_best_threshold is not None:
            best_threshold = best_threshold + ((curr_max_best_threshold - best_threshold) / 2.0)

        print("Best F1:", max_f1, best_threshold)
        self.classification_threshold = best_threshold

        return max_f1

def calculate_accuracy(positive_synsets, negative_synsets, predicted_positive_synsets):
    true_positives = positive_synsets.intersection(predicted_positive_synsets)
    true_negativ_synsets = negative_synsets - predicted_positive_synsets

    return (len(true_positives) + len(true_negativ_synsets)) / float(len(positive_synsets.union(negative_synsets)))

def calculate_rec_prec_f1(real_positive_synsets, real_negative_synsets, predicted_positive_synsets):
    true_positives = real_positive_synsets.intersection(predicted_positive_synsets)
    false_positives = predicted_positive_synsets - true_positives

    recall = float(len(true_positives)) / len(real_positive_synsets)

    if (len(true_positives) + len(false_positives)) > 0:
        precision = float(len(true_positives)) / (len(true_positives) + len(false_positives))
    else:
        precision = 0.0

    if (recall + precision) > 0:
        f1 = 2 * (recall * precision) / (recall + precision)
    else:
        f1 = 0.0

    return recall, precision, f1




if __name__ == "__main__":
    concepts = read_visa_dataset(sys.argv[1],
            ["animals", "appliances", "artefacts", "clothing", "food"])
    
    attribute_synset_map = create_attribute_synset_map(concepts)

    all_synsets = set()

    for attrib, synsets in attribute_synset_map.items():
        all_synsets.update(synsets)

    #top_n_attribs = find_top_n_attributes(attribute_synset_map)

    #synset_image_map = load_images_for_synsets(set(list(all_synsets)[:10]))
    synset_embeddings_map = {}

   # if not os.path.isdir("datafiles"):
   #     os.mkdir("datafiles")

   # model = VGG19()
   # output_func = K.function([model.layers[0].input], [model.get_layer("fc2").output])
    
    f_data = h5py.File("visual-embeddings.hdf5", "r")

    for item in f_data:
        fc1 = f_data.get(item, {}).get("fc1")
        if not fc1:
            continue
        synset_embeddings_map[item] = fc1
    print("Loaded", len(synset_embeddings_map), "synsets")

    all_synsets = set(synset_embeddings_map.keys())
    #synset_embeddings_map = dict(map(lambda x: (x, f_data[x]["fc1"]), f_data))

    #for synset in all_synsets:

    #    datafile_path = "datafiles/{}.embeddings.hdf5".format(synset)
    #    if os.path.isfile(datafile_path):
    #        f = h5py.File(datafile_path, "r")
    #        synset_embeddings_map[synset] = f["image_embeddings"][...]
    #    else:
    #        images = load_images_for_synset(synset)
    #        layer_output_train = output_func([images])[0]
    #        
    #        f = h5py.File(datafile_path, "w")
    #        dset = f.create_dataset("image_embeddings", layer_output_train.shape, dtype='f')
    #        dset[...] = layer_output_train
    #        f.close()
    #        synset_embeddings_map[synset] = layer_output_train

    top_n_attribs = map(lambda x: (x[0], x[1]), sorted(attribute_synset_map.items(), key = lambda x: len(x[1]), reverse = False))
    
    performance_scores = []

    if not os.path.isdir("./classifiers"):
        os.mkdir("./classifiers")

    f_results = open("image-results.txt", "w")

    f_results.write("Attribute\t&\tF1\t&\tRecall\t&\tPrecision\\\\\n")

    f_f_scores = open("synset-f1.txt", "w")

    for attrib, synsets in top_n_attribs:
        print("Predicting attrib", attrib)
        synsets = synsets.intersection(all_synsets)
        if len(synsets) == 1:
            continue
        
        negative_synsets = all_synsets - set(synsets)

        print(len(synsets), len(negative_synsets))
        
        if len(negative_synsets) * len(synsets) == 0:
            continue

        postive_synsets_train, positive_synsets_test = \
                train_test_split_synsets(synsets)
        negative_synsets_train, negative_synsets_test = \
                train_test_split_synsets(negative_synsets)


        if len(positive_synsets_test) * len(negative_synsets_test) == 0:
            continue

        clf = AttributeClassifier()
        clf.fit(synset_embeddings_map, synsets, negative_synsets)
        image_f1, image_recall, image_precision = clf.evaluate_classifier_performance(synset_embeddings_map, positive_synsets_test, negative_synsets_test)
        synset_f1 = clf.tune_threshold(synset_embeddings_map, positive_synsets_test, negative_synsets_test)

        f_f_scores.write("{}\t&\t{}\n".format(attrib, synset_f1))
        f_results.write("{}\t&\t{}\t&\t{}\t&\t{}\\\\ \n".format(attrib, image_f1, image_recall, image_precision))

        joblib.dump(clf, 'classifiers/{}.clf'.format(attrib))

        f_results.flush()
        f_f_scores.flush()

        performance_scores.append((image_f1, image_recall, image_precision))

    sums = [0] * 3

    for tp in performance_scores:
        for idx in range(3):
            sums[idx] += tp[idx]

    avg_scores = tuple(map(lambda x: x / float(len(performance_scores)), sums))
        

    f_results.write("{}\t&\t{}\t&\t{}\t&\t{}\\\\ \n".format("Total", *avg_scores))
    f_results.close()
    f_f_scores.close()
