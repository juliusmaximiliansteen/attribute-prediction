from __future__ import print_function

import os
import h5py

import sys
import random

from sklearn.externals import joblib

from attribpredict.predictor import AttributeClassifier
from attribpredict.visaparser import read_visa_dataset
from attribpredict.embeddingcreator import create_embeddings
from attribpredict.attribmap import map_attribute_predictions_to_assertions


if __name__ == "__main__":
    candidates = {
        ("n02324587", "Rabbit"),
        ("n02128669", "Panther"),
        ("n02431122", "Deer"),

        ("n04233124", "skyscraper"),
        ("n03267113", "Eight ball"),
        ("n02882647", "Bowling pin"),

        ("n02846141", "Black"),
        ("n02841847", "Bio hazard suit"),
        ("n03492922", "Hard hat"),

        ("n07697537", "Hot dog"),
        ("n07727048", "Kidney bean"),
        ("n07714802", "Cabbage")
    }

    concepts = read_visa_dataset(sys.argv[1],
            ["animals", "appliances", "artefacts", "clothing", "food"])
    
    all_synsets = set(map(lambda x: x[1], concepts.keys()))

    all_attribs = set()

    for synset, attribs in concepts.items():
        all_attribs.update(attribs)


    #print("\n".join(sorted(all_attribs)))

    candidate_ids = set(map(lambda x: x[0], candidates))
    
    #print(candidate_ids.intersection(all_synsets))

    create_embeddings("training-embeddings.hdf5", candidate_ids)

    f_data = h5py.File("training-embeddings.hdf5", "r")

    synset_embeddings_map = {}
    for item in f_data:
        fc1 = f_data.get(item, {}).get("fc1")
        if not fc1:
            continue
        synset_embeddings_map[item] = fc1
    print("Loaded", len(synset_embeddings_map), "synsets")

    attrib_clfs = {}

    attribute_clf_count = 0
    for attrib_clf_file in os.listdir("classifiers"):
        attrib, _ = attrib_clf_file.rsplit(".", 1)
        clf = joblib.load(os.path.join("classifiers", attrib_clf_file))

        attrib_clfs[attrib] = clf

    candidate_dict = dict(candidates)

    prediction_scores = []
    for attrib, clf in sorted(attrib_clfs.items(), key = lambda it: len(it[1].positive_synsets), reverse = True):
        members_and_scores = clf.predict_classification(synset_embeddings_map, candidate_ids, use_fixed_threshold = False, include_score = True)

        if len(members_and_scores) == 0:
            members = []
        else:
            members = list(zip(*members_and_scores))[0]
        print(members)

        print(attrib)
        prediction_scores.extend(map(lambda x: ((attrib, x[0]), x[1]), members_and_scores))
        print("\n".join(map(lambda x: candidate_dict[x], members)))
        print("")

        attribute_clf_count += 1

    print("Total synsets:", len(all_synsets))
    print("Total attribs:", len(all_attribs))
    print("Attribs with clf:", attribute_clf_count)


    print("")
    print("")

    print(len(prediction_scores))
    prediction_scores = sorted(prediction_scores, key = lambda x: x[1], reverse = True)

    assertions = map_attribute_predictions_to_assertions(map(lambda x: x[0], prediction_scores))

    with open("assertions.txt", "w") as f_assertions:
        for assertion_idx in range(len(assertions)):
            assertion = assertions[assertion_idx]
            f_assertions.write("{} {} {} ({})\n".format(assertion[0], assertion[1], assertion[2], prediction_scores[assertion_idx][1]))


