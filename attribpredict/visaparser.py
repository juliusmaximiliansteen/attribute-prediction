import xml.sax

import os

def read_visa_dataset(basepath, include_list = []):
    all_concepts = {}
    for filename in os.listdir(basepath):
        if filename.endswith(".xml"):
            if len(include_list) > 0 and not any(map(lambda x: filename.lower().startswith(x), include_list)):
                continue
            full_filename = os.path.join(basepath, filename)
            all_concepts.update(read_visa_dataset_file(full_filename))

    return all_concepts

def read_visa_dataset_file(filename):
    handler = VisaParser()
    xml.sax.parse(filename, handler)

    return handler.all_concepts

class VisaParser(xml.sax.handler.ContentHandler):
    def __init__(self):
        self.all_concepts = {}

        self.current_concept = None
        self.current_attribute_characters = ""
        self.current_prefix = ""

    def startElement(self, name, attr):
        if name == "concept":
            self.current_concept = (attr["name"], attr.get("synsetID"))
        elif name != "no_evidence" and name != "concepts":
            self.current_prefix = name


    def characters(self, content):
        if self.current_concept is not None:
            self.current_attribute_characters += content
        

    def endElement(self, name):
        if name == "concept":
            filtered_attribs = filter(lambda x: len(x) > 0, map(lambda x: x.strip(), self.current_attribute_characters.strip().split("\n")))
            prefixed_attribs = map(lambda x: self.current_prefix + "." + x, filtered_attribs)
            self.all_concepts[self.current_concept] = list(prefixed_attribs)
            self.current_concept = None
            self.current_attribute_characters = ""
            self.current_prefix = ""
