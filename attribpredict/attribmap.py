import sys
from collections import namedtuple

from nltk.stem import WordNetLemmatizer

from attribpredict.visaparser import read_visa_dataset

import requests

from nltk.corpus import wordnet as wn

Placeholder = namedtuple("Placeholder", "key")


PREFIX_RULES = {
    "is_": ("/r/HasProperty", Placeholder("synset"), Placeholder("rule_content_uri")),
    "made_of_": ("/r/MadeOf", Placeholder("synset"), Placeholder("rule_content_uri")),
    "has_": ("/r/HasA", Placeholder("synset"),  Placeholder("rule_content_uri"))
}

def attributes_to_rule_templates(attribs):
    templates = {}
    lemmatizer = WordNetLemmatizer()

    for full_attrib in attribs:
        attrib_prefix, attrib = full_attrib.split(".")
        for prefix, template in PREFIX_RULES.items():
            if attrib.startswith(prefix):
                rule_content = attrib[len(prefix):]

                if prefix == "is_":
                    pos = "a"
                else:
                    pos = "n"
                rule_content_uri = word_to_concept_net_uri(rule_content, pos)

                template = rule_by_filling_placeholder(template, "rule_content_uri", rule_content_uri)

                templates[full_attrib] = template
        
        if attrib.startswith("beh_-_"):
            rule_content = attrib[len("beh_-_"):]
            words = rule_content.split("_")

            lemmatized = lemmatizer.lemmatize(words[0])

            rule_content_uri = word_to_concept_net_uri(lemmatized, "v")
            template = rule_by_filling_placeholder((
                "/r/CapableOf",
                Placeholder("synset"),
                Placeholder("rule_content_uri")
            ), "rule_content_uri", rule_content_uri)
            

            templates[full_attrib] = template

    return templates

def word_to_concept_net_uri(word, pos):
    return "/c/en/{}/{}".format(word, pos)


def rule_by_filling_placeholder(template, placeholder_key, filler):
    new_rule = []

    for elem in template:
        if isinstance(elem, Placeholder) and elem.key == placeholder_key:
            new_rule.append(filler)
        else:
            new_rule.append(elem)

    return tuple(new_rule)


def map_wordnet_offset_and_pos_to_concept_net_uri(wordnet_id, use_fallback = True):
    pos = wordnet_id[0]
    numeric_id = wordnet_id[1:]

    response = requests.get("http://api.conceptnet.io/query?end=http://wordnet-rdf.princeton.edu/wn31/{numeric}-{pos}".format(numeric = numeric_id, pos = pos)).json()

    if len(response["edges"]) > 0:
        if len(response["edges"]) > 1:
            print("WARNING: More than one concept linked to WordNet-Id")
        print(wordnet_id, response["edges"][0]["start"]["@id"])
        return response["edges"][0]["start"]["@id"]

    if not use_fallback:
        return None
    else:
        uri = word_to_concept_net_uri(wn._synset_from_pos_and_offset(pos, int(numeric_id)).lemmas()[0].name(), "n")
        return uri


def map_attribute_predictions_to_assertions(predictions):
    all_attribs = set(map(lambda x: x[0], predictions))

    templates = attributes_to_rule_templates(all_attribs)

    assertions = []

    for attrib, synset in predictions:
        attrib_template = templates.get(attrib)
        if attrib_template is None:
            continue
        synset_concept_net_url = map_wordnet_offset_and_pos_to_concept_net_uri(synset)

        assertions.append(rule_by_filling_placeholder(attrib_template, "synset", synset_concept_net_url))

    return assertions
    

if __name__ == "__main__":
    #print(map_wordnet_offset_and_pos_to_concept_net_uri("n102123649"))
    dataset = read_visa_dataset(sys.argv[1])
    all_attribs = sorted([item for l in dataset.values() for item in l])

    attributes_to_rule_templates(all_attribs)

