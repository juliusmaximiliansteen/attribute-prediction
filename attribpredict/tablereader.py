import math
import sys

def avg(l):
    return float(sum(l)) / len(l)

if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        f1_scores = []
        rec_scores = []
        prec_scores = []

        next(f)
        for line in list(f)[-6:]:
            components = line.split("&")
            f1 = float(components[1].strip())
            f1_scores.append(f1)

            prec = float(components[3].strip().strip("\\"))
            prec_scores.append(prec)

            rec = float(components[2].strip())
            rec_scores.append(rec)

            print components[0], "&", " & ".join(map(lambda x: str(round(x, 3)), (f1, rec, prec)))

        print("&".join(map(lambda x: str(round(avg(x[:-1]), 3)), (f1_scores, rec_scores, prec_scores))))
            



