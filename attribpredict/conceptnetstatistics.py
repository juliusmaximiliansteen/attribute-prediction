import requests

def count_has_color_edges(color):
    url = "http://api.conceptnet.io/query?rel=/r/HasProperty&end=/c/en/{color}".format(color = color)
    
    edge_count = 0

    while True:
        response = requests.get(url).json()
        edge_count += len(response["edges"])

        if response.get("view", {}).get("paginatedProperty") == "edges":
            next_page = response.get("view", {}).get("nextPage")

            if not next_page:
                break

            url = "http://api.conceptnet.io" + next_page
        else:
            break
        
    return edge_count

if __name__ == "__main__":
    colors = [
        "red",
        "green",
        "blue",
        "white",
        "black",
    ]

    for color in colors:
        print(color, count_has_color_edges(color))

