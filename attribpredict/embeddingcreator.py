import os
import shutil

import sys
import tarfile

import threading

from keras.applications import VGG19
import keras.backend as K
import theano

import h5py
import skimage.io
import skimage.transform
import numpy as np

import requests

from attribpredict.visaparser import read_visa_dataset
from attribpredict.predictor import create_attribute_synset_map

import time



def read_images(image_filenames, dims = (224, 224)):
    all_images = []

    for fname in image_filenames:
        img = skimage.io.imread(fname)
        if len(img.shape) == 2:
            continue
        img = skimage.transform.resize(img, dims)
        img = img.reshape((dims[0], dims[1], 3))
        all_images.append(np.array(img, dtype = theano.config.floatX))

    images = np.stack(all_images)
    images = np.transpose(images, [0, 3, 2, 1])
    print("Loaded {} training samples".format(images.shape[0]))
    return images


def download_and_unpack_imagenet(wn_id):
    if os.path.isdir(wn_id):
        return wn_id
    response = requests.get('http://www.image-net.org/download/synset?wnid={wn_id}&username=juliussteen&accesskey=f5129c01d8a2fdae157b33a4b4df5c0994e123a5&release=latest&src=stanford'.format(wn_id = wn_id), stream=True)
    
    temp_tar_fname = "{}.temp.tar".format(wn_id)
    with open(temp_tar_fname, 'wb') as f_out:
        for chunk in response.iter_content(chunk_size=2048):
            f_out.write(chunk)

    target_dirname = "{}".format(wn_id)

    os.mkdir(target_dirname)
    
    tar = tarfile.open(temp_tar_fname, "r")
    tar.extractall(target_dirname)

    os.remove(temp_tar_fname)

    return target_dirname

def get_image_files_in_dir(directory):
    img_filenames = []
    for filename in os.listdir(directory):
        if filename.endswith(".JPEG"):
            img_filenames.append(os.path.join(directory, filename))

    return img_filenames


def run_batched_keras_function(func, arr_in, batch_size = 8):
    results = []
    for index in range(0, arr_in.shape[0], batch_size):
        results.append(func([arr_in[index:index+batch_size]]))
    
    return map(np.concatenate, zip(*results))


class DownloadThread(threading.Thread):
    def __init__(self, concepts):
        self.concepts = concepts
        print(self.concepts)
        threading.Thread.__init__(self)
        

    def run(self):
        for synset in self.concepts:
            print("Downloading {}", synset)
            download_and_unpack_imagenet(synset)

            while len(list(filter(lambda x: x.startswith("n"), os.listdir(".")))) >= 5:
                time.sleep(1)

def create_embeddings(outfile_name, all_synsets):
    model = VGG19()
    output_func_fcs = K.function([model.layers[0].input], [model.get_layer("fc1").output, model.get_layer("fc2").output])

    with h5py.File(outfile_name, "a") as f_data:
        all_synsets = all_synsets - set(f_data.keys())
        thread = DownloadThread(all_synsets)
        thread.deamon = True
        thread.start()
        for synset in all_synsets:
            start_time = time.time()
            #if synset in f_data.keys():
            #    print("Skipping...")
            #    continue
            synset_group = f_data.create_group(synset)

            #print("Downloading...")

            while not os.path.isdir(synset):
                time.sleep(1)
            images_dirname = synset
            img_filenames = get_image_files_in_dir(images_dirname)

            images = read_images(img_filenames[:500])

            print("Predicting...")
            #layer_output_fc1, layer_output_fc2 = output_func_fcs([images])
            layer_output_fc1, layer_output_fc2 = run_batched_keras_function(output_func_fcs, images)
            
            print("Writing...")
            fc1_dset = synset_group.create_dataset("fc1", layer_output_fc1.shape, dtype='f')
            fc1_dset[...] = layer_output_fc1
            layer_output_fc1 = None

            fc2_dset = synset_group.create_dataset("fc2", layer_output_fc2.shape, dtype='f')
            fc2_dset[...] = layer_output_fc2
            print("Done writing.")

            shutil.rmtree(images_dirname)
            print(images_dirname)
            print(int(start_time - time.time()))
    

if __name__ == "__main__":
    concepts = read_visa_dataset(sys.argv[1],
            ["animals", "appliances", "artefacts", "clothing", "food"]
    )

    attribute_synset_map = create_attribute_synset_map(concepts)
    

    all_synsets = set()
    
    for attrib, synsets in attribute_synset_map.items():
        all_synsets.update(synsets)

    print(len(all_synsets))


    model = VGG19()
   # output_func_fc1 = K.function([model.layers[0].input], [model.get_layer("fc1").output])
   # output_func_fc2 = K.function([model.layers[0].input], [model.get_layer("fc2").output])
    output_func_fcs = K.function([model.layers[0].input], [model.get_layer("fc1").output, model.get_layer("fc2").output])

    with h5py.File("visual-embeddings.hdf5", "a") as f_data:
        all_synsets = all_synsets - set(f_data.keys())
        thread = DownloadThread(all_synsets)
        thread.deamon = True
        thread.start()
        for synset in all_synsets:
            start_time = time.time()
            #if synset in f_data.keys():
            #    print("Skipping...")
            #    continue
            synset_group = f_data.create_group(synset)

            #print("Downloading...")

            while not os.path.isdir(synset):
                time.sleep(1)
            images_dirname = synset
            img_filenames = get_image_files_in_dir(images_dirname)

            images = read_images(img_filenames[:500])

            print("Predicting...")
            #layer_output_fc1, layer_output_fc2 = output_func_fcs([images])
            layer_output_fc1, layer_output_fc2 = run_batched_keras_function(output_func_fcs, images)
            
            print("Writing...")
            fc1_dset = synset_group.create_dataset("fc1", layer_output_fc1.shape, dtype='f')
            fc1_dset[...] = layer_output_fc1
            layer_output_fc1 = None

            fc2_dset = synset_group.create_dataset("fc2", layer_output_fc2.shape, dtype='f')
            fc2_dset[...] = layer_output_fc2
            print("Done writing.")

            shutil.rmtree(images_dirname)
            print(images_dirname)
            print(int(start_time - time.time()))


    sys.exit(0)
    
