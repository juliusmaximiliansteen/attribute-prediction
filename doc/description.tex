\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{caption}

\usepackage{natbib}
\usepackage{algorithm,algorithmic}

\usepackage{multirow}

\usepackage{graphicx}
\usepackage{csquotes}
\usepackage[begintext=``, endtext='']{quoting}
\SetBlockEnvironment{quoting}
\SetBlockThreshold{1} 

\title{Using Images to Predict Attributes for Commonsense Knowledge Bases}
\date{\today}
\author{Julius Steen}

\begin{document}

\maketitle

\section{Introduction}

Many system try to obtain general knowledge about the real world by extracting statements from written text (see for example \citet{knext}).
However, obtaining such commonsense information in an automatic fashion from written text can be hindered by a phenomenon called reporting bias. \citet{reportingbias} have shown empirically that events that are infrequent in the real world are mentioned disproportionally often in text. Systems which rely on events in texts being a representation of the real world are thus exposed to a significant bias which might lead to them assuming that a plane, for example, is more likely to crash than to land safely.
\citeauthor{reportingbias} explain this bias with the human authors' adherence to the conversational maxims formulated by \citet{paulgrice} which mandate coincise communication and thus discourage reiteration of knowledge the reader would be expected to already have. This knowledge, however, is also the kind of information we wish to integrate into a commonsense knowledge base.
While methods have been proposed to circumvent the issue directly in text, for example by finding instances where a certain usual outcome is explicitly negated \citep{negation}, in this work we try to use visual data (i.e. images) to obtain commonsense knowledge.
We consider images to be an advantageous source of information for this kind of knowledge because they are not subject to an author explicitly describing their relevant content.
More specifically this project focuses on obtaining information about attributes. A precursory study using the API of ConceptNet 5 \citep{conceptnet}, a large scale online knowledge base, shows that the three primary colors red, green and blue only have 64, 62, and 33 incoming \textit{PropertyOf} edges each. This suggests that colors may not be covered very well in ConceptNet so far. We extrapolate this might be the case for other primarily visual attributes as well.

The rest of this report is organized as follows. Section \ref{sec:prior} gives a brief overview over prior work. Afterwards we briefly introduce the resources that are used in this project in section \ref{sec:resources} and describe the approach taken for this project in detail in section \ref{sec:approach}. Finally we report results of a limited evaluation in section \ref{sec:evaluation} and conclude the results in section \ref{sec:conclusion}.

\section{Prior Work} \label{sec:prior}

The idea of using visual information to predict links in knowledge bases is inspired by \citet{stating-the-obvious} who use the Microsoft Coco dataset \citep{coco} and extract common spatial relations between object categories from the bounding boxes provided with the images. In addition they extract and generalize common descriptions of such relations using the manually annotated captions in the corpus. Unlike this work they do not rely on automatically tagged data, but use only the manual annotations present in the dataset instead.

Predicting semantic attributes from raw image data is an active field of research in the computer vision community.
\citet{describing-objects-by-their-attributes} use a traditional vision pipeline with visual words as features. \citet{abdulnabi} use convolutional networks on several attribute prediction datasets and achieve competitive results on a clothing and an animal focused dataset.

In the NLP-community multi-modality has been used to improve various semantic tasks, e.g. semantic similarity \cite{grounded-models} or metaphor classification \citep{whiterabbits}, among other applications. \citet{Baroni} argues that visual information is an important tool to ground distributed semantic models in the real world and give them cognitive plausibility.
To this end \citet{visa} construct a dataset of WordNet \citep{wordnet} synsets annotated with visual attributes. They learn attribute classifiers on their dataset from ImageNet \citep{imagenet} images and use their predictions to augment distributed semantic models.

\section{Resources} \label{sec:resources}

\begin{description}
\item[ImageNet]{ImageNet \citep{imagenet} is a resource that maps the structure of the WordNet database to images. Synsets in ImageNet are associated with collections of images which show objects that fit into that synset. Subsets of ImageNet have been used for object classification shared tasks in the past. \citep{ilsvrc}}
\item[VisA]{The visual attributes dataset (VisA) \citep{visa} is a collection of synsets which were manually annotated with a fixed set of visual attributes. The dataset is inspired by prior work in the area of feature norms by \citet{mcrae}. Feature norms are derived by asking native speakers to describe physical objects using attributes they find intuitively most important for them. \citep{mcrae}}
\item[ConceptNet]{ConceptNet \citep{conceptnet} is a knowledge base designed to include general knowledge, e.g. to provide background knowledge to a computer program. It is organized as a graph. Vertices represent concepts, e.g. concrete objects in the real word. Edges represent relations between these concepts. Edges can either belong to a predefined set of general relations or be derived from natural language text.}
\end{description}

\section{Approach} \label{sec:approach}

Our approach consists of two steps. In the attribute prediction step we use a machine learning system to predict whether a majority of the images belonging to a synset in ImageNet shows a certain attribute or not.
In the assertion step these per synset predictions are mapped to triples corresponding to ConceptNet assertions.
Both steps are described in more detail in the following section.

\subsection{Attribute Prediction} \label{sec:approach:prediction}

Similar to \citet{visa} we frame the problem of predicting attributes as a binary classification problem for each attribute. Training examples are derived per synset from their image collections in ImageNet. For every synset at most 250 images are taken into account.
Our process follows the ideas of \citeauthor{visa}, although we do not use visual words as features but instead employ neural network activations derived from a VGG19 neural network that has been trained on the ILSVRC 2012 dataset by \citet{vgg19}. Such features have been shown to be generic enough to allow for them to be used as generic features and in transfer learning settings \citep{decaf, off-the-shelf}.
We use the activations at the forelast fully connected layer, commonly referred to as fc1, as feature vectors. Each image is thus represented as a 4096 dimensional activation vector.

During training images belonging to synsets which are labeled with the attribute in question are used as positive training examples, while images belonging to synsets which are not labeled with the attribute serve as negative examples. The images are subsampled so that there is an equal number of postive and negative training instances. The dataset is then fed into a linear Support Vector Machine to learn a binary classifier that predicts whether an attribute is present in an image or not. We use the scikit-learn implementation of the Support Vector Machine \citep{sklearn} with default settings.
For each synset a score is calculated based on how many of its images are classified as positive.

While this score could be used together with a fixed threshold, e.g. $0.5$, to determine the classification of the whole synset, we opt to use a held-out set of one fifth of the synsets in the dataset to calculate a dynamic theshold instead. The rational for this is that certain attributes, like the presence of a beak, for example, might not be visible in all possible perspectives of an image. In these cases, a dynamic threshold might allow the system to adapt to the specifics of the attribute.
We calculate F1 scores for different thresholds at $0.1$ intervals between $1.0$ and $0.0$ and choose the threshold that maximizes the score. If a range of values maximizes the score, we choose the midpoint of the range as the final threshold.
A synset is classified as having an attribute if the proportion of images labeled positively by the attribute classifier is larger than the threshold.

\subsection{Assertion Construction}

In order to map the attribute predictions to ConceptNet triples, the attributes are first transformed into templates for ConceptNet assertions which have an opening for a ConceptNet URI related to the synset for which the attribute is predicted.
The templates themselves are generated by exploiting the simple structure of the VisA dataset using very general hand-crafted rules which are listed in table \ref{tab:templaterules}. We use simple heuristics to tag the concepts in the template with ConceptNet part-of-speech tags for better disambiguation.

\begin{table}
\begin{center}\begin{tabular}{|l|l|}
\hline
Attribute Pattern & Tuple Template \\\hline
\verb|has_<attribute>| & \verb|/r/HasProperty <synset> /c/<attribute>/a| \\
\verb|is_<noun>| & \verb|/r/HasA <synset> /c/<noun>/n| \\
\verb|made_of_<noun>| & \verb|/r/MadeOf <synset> /c/<noun>/n| \\
\verb|beh-_-<verb>_<verb phrase>| & \verb|/r/CapableOf <synset> /c/<lemmatized verb>/v| \\\hline
\end{tabular}\end{center}

\caption{List of Assertion Template Rules \label{tab:templaterules}}
\end{table}

Afterwards the synsets themselves are mapped to ConceptNet URIs. This follows a two steps process. In the first step the system looks for a concept that has an external link to the linked data version of WordNet that corresponds to the synset.
If this is not the case, a fallback method selects one of the lemmas of the synset to construct a ConceptNet-URI. While this method seems rather simplistic it is symptomatic of a more general problem: While WordNet is a very fine-grained resource, ConceptNet concepts are natural language phrases and are only rudimentarily disambiguated by coarse part-of-speech tags \citep{conceptnet}.
This makes it generally challenging to map synsets to concepts without the aid of explicit links. However, given that in this case we are mapping from the more specific resource (WordNet) to the less specific one, this simple method is expected to be sufficient.

\section{Evaluation} \label{sec:evaluation}

The system is evaluated in two parts. The first part of the evaluation concerns the performance of the individual image classifiers. In a second experiment we use a small set of synsets that are not present in the original dataset to manually evaluate whether the predicted attributes are plausible. In a third and final experiment we manually evaluate a small set of ConceptNet tuples that have been predicted by the system.
The experiments presented here focus on a subset of the dataset. Only synsets and attributes present in five subcategories of the dataset are included: animals, appliances, artefacts, clothing, food.
In total 257 synsets are used in the experiments. We only use attributes which belong to at least five synsets to allow for held-out dataset construction, which results in 207 attributes for which a classifier is constructed.

\subsection{Classifier Performance}

\subsubsection{Setup}

To evaluate per-image classifier performance we use the classifiers trained on the training data for each attribute on the held-out dataset described above to calculate F1-score, recall and precision. This should provide a general overview over the performance of the system.

We report both individual classifier accuracy for the five attribute classifiers with the most training examples and (macro) average scores for all attributes.

\subsubsection{Results}

\begin{table}
\begin{center}\begin{tabular}{|l|c|c|c|}
\hline
Attribute       &       F1      &       Recall  &       Precision\\\hline
has head	& 0.818 & 0.942 & 0.722 \\
has eyes	& 0.749 & 0.993 & 0.601 \\
is white	& 0.165 & 0.091 & 0.848 \\
eats	& 0.608 & 0.465 & 0.875 \\
is black	& 0.711 & 0.976 & 0.559 \\\hline\hline
Total (all attributes) & 0.818 & 0.849 & 0.818 \\\hline
\end{tabular}\end{center}
\caption{Classifier Performance \label{tab:fscores}}
\end{table}


The results in table \ref{tab:fscores} show adequate performance of the individual classifiers, except the \textit{is white} classifier, which exhibits very low recall. However, as to our knowledge no direct evaluation of classifier performance on a per-image basis has been performed on the VisA dataset, no performance comparison can be made. A general trend that can be observed is that precision tends to be much lower than recall which is an undesirable trait in these classifiers because low precision increases noise in the attribute predictions.
This suggest that the predictions made by the system might require substantial filtering before use. 

\subsection{Attribute Prediction}

\subsubsection{Setup}

The attribute prediction is evaluated on twelve synsets listed in table \ref{tab:synsets} which were not included in the VisA dataset. They were chosen manually based on their similarity to those included in VisA.

\begin{table}
\begin{center}\begin{tabular}{|c|c|c|}
\hline
Rabbit \textit{n02324587} &
Panther \textit{n02128669} &
Deer \textit{n02431122} \\\hline
Skyscraper \textit{n04233124} &
Eight ball \textit{n03267113} &
Bowling pin \textit{n02882647} \\\hline
Black (funeral clothing) \textit{n02846141} &
Bio hazard suit \textit{n02841847} &
Hard hat \textit{n03492922} \\\hline
Hot dog \textit{n07697537} &
Kidney bean \textit{n07727048} &
Cabbage \textit{n07714802} \\\hline
\end{tabular}\end{center}

\caption{List of Synsets used in evaluation. Text in italics references WordNet POS-Tag and offset. \label{tab:synsets}}
\end{table}

Given the limited resources we restrict evaluation to the five attributes with the most synsets. These attributes are: \textit{is black}, \textit{eats}, \textit{is white}, \textit{has eyes}, \textit{has head}.

\subsubsection{Results}

We report all synsets for which the attributes were predicted by the classifiers.

\begin{description}
\item[is black]{Deer, Panther, Skyscraper, Black, Rabbit, Bio hazard suit, Hard hat, Bowling pin}
\item[eats]{Rabbit}
\item[is white]{Rabbit, Skyscraper}
\item[has eyes] {Deer, Rabbit, Panther, Bio hazard suit}
\item[has head] {Deer, Rabbit, Panther}
\end{description}

While limited in their meaningfulness due to the small sample size, the results show several trends. For the very specific attributes \textit{has eyes}, \textit{eats} and \textit{has head} which are only applicable to animals, the system only classifies one synset, the biohazard suit incorrectly as having eyes. We speculate that the strange assertion can in part be explained by observing that most images in the bio hazard suit synset show suits worn by humans whose eyes are often visible. In general the trend to correctly limit the possession of body parts to living things suggests that the system is well capable of discerning whether or not a synset belongs to a category to which a certain attribute may apply. This is unsurprising given that the classification is based around features derived from an object classification network.
Another observation is that the \textit{has black}-attribute is very broad and includes objects which one would not expect to be labeled with this attributes, like bowling pins (which are usually white) and Bio hazard suits (which tend to be white, orange or blue in ImageNet). This is surprising given that colors should usually be easy to detect from images. The phenomenon may thus require a more detailed investigation to determine its cause.

\subsection{Assertion Construction}

\subsubsection{Setup}

As a final experiment we ask the system to output the most confident ConceptNet assertions constructed from all attributes with trained classifiers. The assertion ranking is determined by the scoring defined in section \ref{sec:approach:prediction}.
In total 507 assertions were constructed from the twelve training examples across all attribute classifiers. We take a look at the fifteen most confident ones in the results section.

\subsubsection{Results}

The fifteen most confident predictions are presented in table \ref{tab:assertions}.

\begin{table}
\begin{center}\begin{tabular}{|lll|c|}
\hline
Relation &  Source & Target & Confidence \\\hline
\verb|/r/HasA|&\verb|/c/en/panther/n|&\verb|/c/en/fur/n|& 0.98\\
\verb|/r/HasA|&\verb|/c/en/red_deer/n|&\verb|/c/en/jaws/n|& 0.976\\
\verb|/r/HasProperty|&\verb|/c/en/bunny/n|&\verb|/c/en/white/a|& 0.968\\
\verb|/r/HasA|&\verb|/c/en/hotdog/n|&\verb|/c/en/skin/n|& 0.961\\
\verb|/r/HasA|&\verb|/c/en/panther/n|&\verb|/c/en/claws/n|& 0.96\\
\verb|/r/HasA|&\verb|/c/en/red_deer/n|&\verb|/c/en/ears/n|& 0.96\\
\verb|/r/HasProperty|&\verb|/c/en/black/n|&\verb|/c/en/black/a|& 0.956\\
\verb|/r/HasA|&\verb|/c/en/bunny/n|&\verb|/c/en/fur/n|& 0.956\\
\verb|/r/HasA|&\verb|/c/en/bunny/n|&\verb|/c/en/claws/n|& 0.952\\
\verb|/r/CapableOf|&\verb|/c/en/bunny/n|&\verb|/c/en/drink/v|& 0.94\\
\verb|/r/HasA|&\verb|/c/en/panther/n|&\verb|/c/en/teeth/n|& 0.936\\
\verb|/r/HasProperty|&\verb|/c/en/panther/n|&\verb|/c/en/black/a|& 0.932\\
\verb|/r/HasProperty|&\verb|/c/en/eight_ball/n|&\verb|/c/en/white/a|& 0.932\\
\verb|/r/HasProperty|&\verb|/c/en/bunny/n|&\verb|/c/en/black/a|& 0.924\\
\verb|/r/HasA|&\verb|/c/en/kidney_bean/n|&\verb|/c/en/skin/n|& 0.924\\\hline
\end{tabular}\end{center}
\caption{List of Predicted Assertions \label{tab:assertions}}

\end{table}

One recurring pattern in the most confident predictions is that the system predicts very general statements about animals, e.g. that they have fur and ears, with high confidence.
However, some results, like the rabbit being classified as a clawed animal, suggest that this is a result of the classifiers correctly detecting the category of a synset, e.g. that it is an animal, and not the attributes themselves.
Another curiosity is that both kidney beans and hot dogs are incorrectly predicted to have a skin. As of now, we do not have a satisfying explanation for this phenomenon.

\section{Conclusion and Future Work} \label{sec:conclusion}

Our experiments suggest that it is possible to derive good and useful predictions about attributes from image data. However, there are still several issues that need to be addressed.
Especially the phenomenon that certain predictions seem to be based more on coarse categories like ''animal'' than the specific synset in question can lead to incorrect predictions. In future experiments this could be alleviated by specifically including images from very similar synsets that do not have the attribute in question as negative examples. This might force the classifiers to learn to discriminate better in such cases instead of relying on the categories alone.
Another avenue of improvement for the system could be to directly train neural networks, for example by using a multi-task CNN architecture similar to \citet{abdulnabi}.
However, a more thorough manual evaluation of the results together with the inclusion of the whole dataset in the experiments is required to make more confident statements about the actual performance and shortcomings of the system.

\bibliography{project}
\bibliographystyle{plainnat}

\end{document}
